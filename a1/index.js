const http = require('http')
const port = 4000

let course = [
  {
    courseName: 'Introduction to GIT 101',
    instructor: 'Gordon Ramsay',
  },
  {
    courseName: 'Introduction to JavaScript 102',
    instructor: 'Kuya Kim Atienza',
  },
  {
    courseName: 'Introduction to MongoDB 103',
    instructor: 'Mike Enriquez',
  },
  {
    courseName: 'Introduction to Node.js 104',
    instructor: 'Harry Potter',
  },
]

const server = http.createServer((request, response) => {
  if (request.url === '/' && request.method === 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(`Welcome to Booking System`)
  } else if (request.url == '/profile' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(`Welcome to your profile!`)
  } else if (request.url == '/courses' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(`Here's our courses available:`)
  } else if (request.url == '/addcourse' && request.method == 'POST') {
    let requestBody = ''

    request.on('data', (userData) => {
      requestBody += userData
    })
    request.on('end', () => {
      requestBody = JSON.parse(requestBody)
      course.push(requestBody)

      response.writeHead(200, { 'Content-Type': 'text/plain' })
      response.end(`Add a course to our resources`)
    })
  } else if (request.url == '/updatecourse' && request.method == 'PUT') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end('Update a course to our resources')
  } else if (request.url == '/archivecourses' && request.method == 'DELETE') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(`Archive courses to our resources`)
  } else {
    response.writeHead(404, { 'Content-Type': 'text/plain' })
    response.end('ERROR! 404 Webpage not found!')
  }
})

server.listen(port)

console.log(`Server running at localhost: │█║▌║▌║${port}║▌║▌║█│`)
