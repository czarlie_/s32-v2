const http = require('http')
const port = 4000

const server = http.createServer((request, response) => {
  /*
  HTTP Requests are differentiated not only via their endpoints but also with their methods

  HTTP Methods simply tells the server what action it msut take or what kind of response is needed for the request

  With an HTTP Method, we can create routes with the same endpoint but with different methods
 */

  // forgot something
  // method: GET
  if (request.url == '/items' && request.method == 'GET') {
    // requests the '/items' path and 'GETs" information
    response.writeHead(200, { 'Content-type': 'text/plain' })
    // ends the response process
    response.end(`Data retrieved from the database! (^V^)`)
  } else if (request.url == '/items' && request.method == 'POST') {
    // requests the '/items' path and 'SENDs' information
    response.writeHead(200, { 'Content-type': 'text/plain' })
    // ends the response process
    response.end(`Data to be sent to the database! (^w^)`)
  }
})

server.listen(port)

console.log(`Server running at localhost: │█║▌║▌║${port}║▌║▌║█│`)
