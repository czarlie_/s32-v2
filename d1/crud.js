const http = require('http')
const port = 4000

// Mock Database
let directory = [
  {
    name: 'Ash Ketchum',
    email: 'besttrainer@mail.com',
  },
  {
    name: 'Jose Masipag',
    email: 'bawalangtamad@mail.com',
  },
  {
    name: 'Hanamichi Sakuragi',
    email: 'haringrebound@mail.com',
  },
]

const server = http.createServer((request, response) => {
  if (request.url == '/' && request.method == 'GET') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(
      `This is a response to a GET method request for the '/' endpoint (^V^)`
    )
  } else if (request.url == '/' && request.method == 'POST') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(
      `This is a response to a POST method request for the '/' endpoint (^V^)>`
    )
  } else if (request.url == '/' && request.method == 'PUT') {
    response.writeHead(200, { 'Content-type': 'text/plain' })
    response.end(
      `This is a response to a POST method request for the '/' endpoint (^w^)>`
    )
  } else if (request.url === '/' && request.method === 'DELETE') {
    response.writeHead(200, { 'Content-Type': 'text/plain' })
    response.end(
      `This is a response to a POST method request for the '/' endpoint <(^O^)>`
    )
  }
  // Get Users
  // Route for returning all users upon receiving a GET request
  else if (request.url == '/users' && request.method == 'GET') {
    // set the status code to 200, denoting OK
    // set response output to JSON data type
    response.writeHead(200, { 'Content-Type': 'application/json' })
    // input has to be STRING data type hence we use the JSON.stringiy() method
    // this string input will be converted to desired output data type which has been set to JSON
    response.write(JSON.stringify(directory))
    response.end()
  }
  // Route to add a new user?
  // We have to receive an input from the client
  else if (request.url == '/users' && request.method == 'POST') {
    // in Node.js this is done in two steps:
    // This will act as a placeholder for the resource or data to be created by the client later on
    let requestBody = ''
    // 1st step: data step
    // data step will read the incoming stream of data from the client and process it so we can save it in the requestBody variable
    request.on('data', (userData) => {
      requestBody += userData
      console.log(userData)
    })
    // 2nd step - this will run once or after the request data has been completely sent from the client
    request.on('end', () => {
      requestBody = JSON.parse(requestBody)
      console.log(requestBody)
      directory.push(requestBody)
      console.log(directory)
      response.writeHead(200, { 'Content-Type': 'application/json' })
      response.end(JSON.stringify(directory))
    })
  } else {
    request.writeHead(404, { 'Content-type': 'text/plain' })
    request.end(`ERROR! 404 page not found! (-_-)`)
  }
})

server.listen(port)

console.log(`Server is running via localhost: │█║▌║▌║${port}║▌║▌║█│`)

/*
Mini Activity #1

Create 4 new routes for the following endpoint: "/"

1. This route should be a "GET" method route
    -Add our status code, 200 and our Content Type: text/plain.
    -Add an end() method to end the response with the following message:
    "This is a response to a GET method request for the / endpoint."

2. This route should be a "POST" method route
    -Add our status code, 200 and our Content Type: text/plain.
    -Add an end() method to end the response with the following message:
    "This is a response to a POST method request for the / endpoint."

3. This route should be a "PUT" method route
    -Add our status code, 200 and our Content Type: text/plain.
    -Add an end() method to end the response with the following message:
    "This is a response to a PUT method request for the / endpoint."

4. This route should be a "DELETE" method route
    -Add our status code, 200 and our Content Type: text/plain.
    -Add an end() method to end the response with the following message:
    "This is a response to a DELETE method request for the / endpoint."

-Use Postman and create request appropriately to test your routes.
What’s the meaning of 'application/json'?
*/
